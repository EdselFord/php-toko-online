<?php
include "header.php";
include "database/db.php";

if (!isset($_SESSION['username'])) header("Location: user_login.php");

$kdcs = $_SESSION['kd_cs'];
$daftar_pesanan = $koneksi->query("SELECT * FROM keranjang WHERE kode_customer='$kdcs'")->fetch_all();
?>

<div class="container" style="margin-bottom: 20px;">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Checkout</b></h2>

  <div style="width: 40vw">
    <h2>Daftar Pesanan</h2>
    <table class="table">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Harga</th>
          <th>Qty</th>
          <th>Sub Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 0;
        foreach ($daftar_pesanan as [$id, $kdcs, $kode_produk, $nama_produk, $qty, $harga_total]) :
          $harga = $koneksi->query("SELECT harga FROM produk WHERE kode_produk='$kode_produk'")->fetch_assoc()['harga'];
          $no++;
        ?>
          <tr>
            <td><?= $no ?></td>
            <td><?= $nama_produk ?></td>
            <td><?= $harga ?></td>
            <td><?= $qty ?></td>
            <td><?= $harga_total ?></td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>

    <div class="alert alert-success" role="alert">
      <b>Pastikan pesanan anda sudah benar</b>
    </div>
    <div class="alert alert-warning" role="alert">
      <b>Silahkan isi form dibawah ini</b>
    </div>
  </div>

  <form action="proses/proses_checkout.php" method="POST">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputPassword1">Nama</label>
          <input type="text" class="form-control" id="exampleInputPassword1" name="nama" value="<?= $_SESSION['username'] ?>" disabled>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Provinsi</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="provinsi" name="provinsi" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Kota</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="kota" name="kota" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Alamat</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="alamat" name="alamat" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Kode Pos</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="kodepos" name="kodepos" required>
        </div>
      </div>
    </div>
    <button type="submit" name="checkout" class="btn btn-success">Order Sekarang</button>
    <a href="keranjang.php" class="btn btn-danger">Cancel</a>
  </form>
</div>


<?php
include "footer.php";
?>