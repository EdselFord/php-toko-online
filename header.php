<?php 
session_start();
include "database/db.php";

if (isset($_SESSION['username'])) {
$kdcs = $_SESSION['kd_cs'];
$count = $koneksi->query("SELECT COUNT(*) AS count FROM keranjang WHERE kode_customer = '$kdcs'")->fetch_assoc()['count'];
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>SKARIGA STORE</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <!-- <link rel="stylesheet" type="text/css" href="css/style.css"> -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>

<body>
  <div class="container-fluid">
    <div class="row top">
      <center>
        <div class="col-md-4" style="padding: 3px;">
          <span> <i class="glyphicon glyphicon-earphone"></i> +6281936556218</span>
        </div>
        <div class="col-md-4" style="padding: 3px;">
          <span><i class="glyphicon glyphicon-envelope"></i> skarigastore@gmail.com</span>
        </div>
        <div class="col-md-4" style="padding: 3px;">
          <span>Skariga Store Indonesia</span>
        </div>
      </center>
    </div>
  </div>


  <nav class="navbar navbar-default" style="padding: 5px;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="." style="color: #ff8680"><b>SKARIGA STORE</b></a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">Home</a></li>
          <li><a href="keranjang.php"><i class="glyphicon glyphicon-shopping-cart"></i> <b><?= isset($_SESSION['username']) ? "[$count]" : ""?></b></a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-user"></i> <?= 
            isset($_SESSION['username']) 
            ?  $_SESSION['username'] 
            : 'Akun' ?> 
            <span class="caret"></span></a> 
            <ul class="dropdown-menu">
              <?php if (!isset($_SESSION['username'])): ?>
              <li><a href="user_login.php">Login</a></li>
              <li><a href="register.php">Register</a></li>
              <?php else: ?>
              <li><a href="proses/proses_logout.php">Logout</a></li>
              <?php endif?>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>