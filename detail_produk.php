<?php
include 'header.php';
include 'database/db.php';

$kode = mysqli_real_escape_string($koneksi, $_GET['produk']);
$produk = $koneksi->query("SELECT * FROM produk WHERE kode_produk='$kode'")->fetch_assoc();
?>
<div class="container">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Detail produk</b></h2>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail"> <img src="image/produk/<?=$produk['image']?>" width="400">
      </div>
    </div>
    <div class="col-md-8">
      <form action="proses/add.php" method="GET">
        <input type="hidden" name="produk" value="<?=$produk['kode_produk']?>">
        <table class="table table-striped">
          <tbody>
            <tr>
              <td><b>Nama</b></td>
              <td><?= $produk['nama'] ?></td>
            </tr>
            <tr>
              <td><b>Harga</b></td>
              <td>Rp.<?= number_format($produk['harga'], 0, ',', '.') ?></td>
            </tr>
            <tr>
              <td><b>Deskripsi</b></td>
              <td><?= $produk['deskripsi'] ?></td>
            </tr>
            <tr>
              <td><b>Jumlah</b></td>
              <td><input class="form-control" type="number" min="1" name="jml" value="1" style="width: 155px;"></td>
            </tr>
          </tbody>
        </table>
        
        <?php if (isset($_SESSION['username'])):?>
        <button type="submit" class="btn btn-success">
          <i class="glyphicon glyphicon-shopping-cart"></i>
          Tambahkan ke Keranjang
        </button>
        <?php else: ?>
        <a href="keranjang.php" class="btn btn-success">
          <i class="glyphicon glyphicon-shopping-cart"></i>
          Tambahkan ke Keranjang
        </a>
        <?php endif ?>
        <a href="index.php" class="btn btn-warning"> Kembali Belanja</a>
      </form>
    </div>
  </div>
</div>
<br>
<br>
<?php
include 'footer.php';
?>