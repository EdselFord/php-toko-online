<footer style="border-top: 4px solid #ff8680; ">
  <div class="container" style="padding-bottom: 50px;">
    <div class="row">
      <div class="col-md-4">
        <h3 style="color: #ff8680"><b>SKARIGA STORE</b></h3>
        <p>Jl. Tlogomas</p>
        <p><i class="glyphicon glyphicon-earphone"></i> +6281936556218</p>
        <p><i class="glyphicon glyphicon-envelope"></i> skarigastore@gmail.com</p>
      </div>
    </div>
  </div>
</footer>
</body>

</html>