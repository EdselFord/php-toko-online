<?php
include "../database/db.php";

if (!isset($_POST["register"])) die();

$nama = $_POST['nama'];
$email = $_POST['email'];
$username = $_POST['username'];
$telp = $_POST['telp'];
$password = $_POST['password'];
$konfirm = $_POST['konfirmasi'];

if ($password != $konfirm) {
  echo "<script>
  alert('password dan konfirmasi tidak sama');
  window.location.href = '../register.php'
</script>";
  die();
}

$password = md5($password);

$cek = $koneksi->query("SELECT * FROM customer WHERE username='$username'");

if ($cek->num_rows != 0) {
  echo "<script>
  alert('username sudah digunakan');
  window.location.href = '../register.php'
</script>";
  die();
}


$kdcs = $koneksi->query("SELECT kode_customer FROM customer ORDER BY kode_customer DESC LIMIT 1")->fetch_assoc();

$number = (int) substr($kdcs['kode_customer'], 1);

$kode = "C" . str_pad($number + 1, 3, "0", STR_PAD_LEFT);

$koneksi->query("INSERT INTO customer VALUES ('$kode', '$nama', '$email', '$username', '$password', '$telp')");

echo "<script>
  alert('akun berhasil dibuat');
  window.location.href = '../user_login.php'
</script>";
