<?php
include "../database/db.php";
session_start();

if (!isset($_POST["checkout"])) die();

$provinsi = $_POST['provinsi'];
$kota = $_POST['kota'];
$alamat = $_POST['alamat'];
$kodepos = $_POST['kodepos'];
$kode_customer = $_SESSION['kd_cs'];

// insert data checkout
$koneksi->query("INSERT INTO checkout VALUES (0, '$kode_customer', '$provinsi', '$kota', '$alamat', '$kodepos', 'pending')");

// mengambil id terakhir
$id = $koneksi->query("SELECT id_checkout FROM checkout ORDER BY id_checkout DESC LIMIT 1")->fetch_assoc()['id_checkout'];

// mengambil data keranjang
$keranjang = $koneksi->query("SELECT * FROM keranjang WHERE kode_customer='$kode_customer'")->fetch_all();

foreach($keranjang as [$id_keranjang, $kdcs, $kode_produk, $nama_produk, $qty, $harga]) {
  $koneksi->query("INSERT INTO checkout_produk VALUES (0, '$id', '$kode_customer', '$kode_produk', '$nama_produk', '$qty', '$harga')");
}

$koneksi->query("DELETE FROM keranjang WHERE kode_customer='$kode_customer'");

echo "<script>
  alert('pesanan sudah di checkout');
  window.location.href = '../keranjang.php'
</script>";