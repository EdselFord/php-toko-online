<?php
include "../database/db.php";
session_start();

if (!isset($_GET['produk'])) die();

$produk = $_GET['produk'];
$kdcs = $_SESSION['kd_cs'];


$data_produk = $koneksi->query("SELECT * FROM produk WHERE kode_produk='$produk'")->fetch_assoc();

$cek_duplicate = $koneksi->query("SELECT * FROM keranjang WHERE kode_customer='$kdcs' AND kode_produk='$produk'")->fetch_all();

if (!empty($cek_duplicate)) {
  $qty = $cek_duplicate[0][4];
  if (isset($_GET['jml'])) $qty += $_GET['jml'];
  else $qty += 1;

  $harga = $qty * $data_produk['harga'];

  $koneksi->query("UPDATE keranjang SET qty='$qty', harga='$harga' WHERE kode_customer='$kdcs' AND kode_produk='$produk'");

  header("Location: ../keranjang.php");
  die();
}

$qty = 1;
if (isset($_GET['jml'])) {
  $qty = $_GET['jml'];
}

$nama_produk = $data_produk['nama'];
$harga = $qty * $data_produk['harga'];

$koneksi->query("INSERT INTO keranjang VALUES (0, '$kdcs', '$produk', '$nama_produk', '$qty', '$harga')");

header("Location: ../keranjang.php");