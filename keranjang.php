<?php
include "header.php";
include "database/db.php";

if (!isset($_SESSION['username'])) header("Location: user_login.php");

$kdcs = $_SESSION['kd_cs'];
$produk_keranjang = $koneksi->query("SELECT * FROM keranjang WHERE kode_customer='$kdcs'")->fetch_all();
?>

<div class="container">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Keranjang</b></h2>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
        <th>Image</th>
        <th>Nama</th>
        <th>Harga</th>
        <th>Qty</th>
        <th>Sub-total</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $total = 0;
      $no = 0;
      foreach ($produk_keranjang as [$id, $kdcs, $kode_produk, $nama_produk, $qty, $harga]) :
        $produk = $koneksi->query("SELECT harga, image FROM produk WHERE kode_produk='$kode_produk'")->fetch_assoc();
        $harga_produk = $produk['harga'];
        $image = $produk['image'];
        $total += $harga;
        $no++;
      ?>
        <form action="proses/edit.php" method="GET">
          <input type="hidden" name="harga" value="<?= $harga_produk ?>">
          <input type="hidden" name="id" value="<?= $id ?>">
          <tr>
            <td><?= $no ?></td>
            <td><img width="200px" height="100px" style="object-fit: cover;" src="image/produk/<?= $image ?>"></td>
            <td><?= $nama_produk ?></td>
            <td>Rp.<?= number_format($harga_produk) ?></td>
            <td><input type="number" name="qty" value="<?= $qty ?>"></td>
            <td>Rp.<?= number_format($harga) ?></td>
            <td style="width: 200px">
              <button type="submit" class="btn btn-warning">
                <i class="glyphicon glyphicon-pencil"></i>
              </button>
              <a href="proses/hapus.php?id=<?= $id ?>" class="btn btn-danger">
                <i class="glyphicon glyphicon-trash"></i>
              </a>
            </td>
          </tr>
        </form>

      <?php endforeach ?>
      
    </tbody>
    <tfoot>
    <tr>
        <th colspan="5">total</th>
        <th>Rp.<?= number_format($total) ?></th>
      </tr>
    </tfoot>
  </table>

  <div style="float: right; margin-bottom: 20px">
    <a href="index.php" class="btn btn-success">Lanjut belanja</a>
    <a href="checkout.php" class="btn btn-warning">Checkout</a>
  </div>
</div>

<?php
include "footer.php";

?>