<?php
include "../../database/db.php";

if (!isset($_POST['edit'])) die();

$kode = $_POST['kode'];
$nama = $_POST['nama'];
$harga = $_POST['harga'];
$deskripsi = $_POST['deskripsi'];
$file_name = $koneksi->query("SELECT image FROM produk WHERE kode_produk='$kode'")->fetch_assoc()['image'];

if ($_FILES['image']['error'] == 0) {
    $target_dir = "../../image/produk/";
    $file_name = str_replace(' ', '', $_FILES['image']['name']);
    $target_file = $target_dir . $file_name;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if (!$check) {
    echo "<script>
        alert('file is not image');
        window.location.href = '../edit.php'
    </script>";
    }

    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
}


$koneksi->query("UPDATE produk SET nama='$nama', image='$file_name', deskripsi='$deskripsi', harga='$harga' WHERE kode_produk='$kode'");

echo "<script>
    alert('produk berhasil di ubah');
    window.location.href = '../m_produk.php'
  </script>";