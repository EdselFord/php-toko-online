<?php 

include "header.php";
include "../database/db.php";

$customer = $koneksi->query("SELECT * FROM customer")->fetch_all();



?>

<div class="container" style="padding-bottom: 200px">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Master Produk</b></h2>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Customer</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 0;
      foreach ($customer as [$kode_customer, $nama, $email]) :
        $no++;
      ?>
          <tr>
            <td><?= $no ?></td>
            <td><?= $kode_customer ?></td>
            <td><?= $nama ?></td>
            <td><?= $email?></td>
            <td style="width: 200px">
              <a href="proses/hapus_customer.php?id=<?= $kode_customer ?>" class="btn btn-danger">
                <i class="glyphicon glyphicon-trash"></i>
              </a>
            </td>
          </tr>

      <?php endforeach ?>
      
    </tbody>
    
  </table>

  <a href="tambah.php" class="btn btn-success">Tambah Produk</a>

</div>

<?php include "footer.php"?>