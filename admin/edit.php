<?php
include "header.php";
include "../database/db.php";

if (!isset($_GET['id'])) header("Location: m_produk.php");

$kode = $_GET['id'];

$produk = $koneksi->query("SELECT * FROM produk WHERE kode_produk='$kode'")->fetch_assoc();

?>

<div class="container" style="padding-bottom: 200px">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Edit Produk</b></h2>


  <form action="proses/edit.php" method="POST" enctype="multipart/form-data">
    <div class="row">
      
      <div class="col-md-6" style="padding-bottom: 10px">
        <img width="200px" height="100px" style="object-fit: cover;" src="../image/produk/<?=$produk['image']?>" alt="">
        <br>
        <label for="fileinput">Pilih Gambar</label>
        <input type="file" name="image" id="fileinput">
        <p style="color: gray">Pilih Gambar untuk Produk</p>

      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Kode Produk</label>
          <input type="text" class="form-control" id="exampleInputPassword1" name="kode" value="<?=$kode?>" readonly>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Nama Produk</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="nama" name="nama" value="<?=$produk['nama']?>" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Harga</label>
          <input type="number" class="form-control" id="exampleInputPassword1" placeholder="harga" name="harga" value="<?=$produk['harga']?>" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputPassword1">Deskripsi</label>
          <textarea class="form-control" id="exampleInputPassword1" placeholder="deskripsi" name="deskripsi" required><?=$produk['deskripsi']?></textarea>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <button type="submit" name="edit" class="btn btn-warning" style="width: 100%">Edit Produk</button>
      </div>
      <div class="col-md-6">
          <a href="m_produk.php" class="btn btn-danger" style="width: 100%">Cancel</a>
      </div>
    </div>
  </form>
</div>

<?php include "footer.php"?>