<?php 

include "header.php";
include "../database/db.php";

$produk = $koneksi->query("SELECT * FROM produk")->fetch_all();



?>

<div class="container" style="padding-bottom: 200px">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Master Produk</b></h2>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>Image</th>
        <th>Harga</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 0;
      foreach ($produk as [$kode_produk, $nama, $image, $deskripsi, $harga]) :
        $no++;
      ?>
          <tr>
            <td><?= $no ?></td>
            <td><?= $kode_produk ?></td>
            <td><?= $nama ?></td>
            <td><img width="200px" height="100px" style="object-fit: cover" src="../image/produk/<?= $image ?>" alt=""></td>
            <td>Rp.<?= number_format($harga) ?></td>
            <td style="width: 200px">
              <a href="edit.php?id=<?= $kode_produk ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-pencil"></i>
              </a>
              <a href="proses/hapus.php?id=<?= $kode_produk ?>" class="btn btn-danger">
                <i class="glyphicon glyphicon-trash"></i>
              </a>
            </td>
          </tr>

      <?php endforeach ?>
      
    </tbody>
    
  </table>

  <a href="tambah.php" class="btn btn-success">Tambah Produk</a>

</div>

<?php include "footer.php"?>