<?php
include "header.php";
include "../database/db.php";

$kode_produk = $koneksi->query("SELECT kode_produk FROM produk ORDER BY kode_produk DESC LIMIT 1")->fetch_assoc()['kode_produk'];

$number = (int) substr($kode_produk, 1);

$kode = "P" . str_pad($number + 1, 3, "0", STR_PAD_LEFT);

?>

<div class="container" style="padding-bottom: 200px">
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680"><b>Tambah Produk</b></h2>


  <form action="proses/add.php" method="POST" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-6" style="padding-bottom: 10px">
        <label for="fileinput">Pilih Gambar</label>
        <input type="file" name="image" id="fileinput">
        <p style="color: gray">Pilih Gambar untuk Produk</p>

      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Kode Produk</label>
          <input type="text" class="form-control" id="exampleInputPassword1" name="kode" value="<?=$kode?>" readonly>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Nama Produk</label>
          <input type="text" class="form-control" id="exampleInputPassword1" placeholder="nama" name="nama" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputPassword1">Harga</label>
          <input type="number" class="form-control" id="exampleInputPassword1" placeholder="harga" name="harga" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputPassword1">Deskripsi</label>
          <textarea class="form-control" id="exampleInputPassword1" placeholder="deskripsi" name="deskripsi" required></textarea>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <button type="submit" name="add" class="btn btn-success" style="width: 100%">Tambah Produk</button>
      </div>
      <div class="col-md-6">
          <a href="m_produk.php" class="btn btn-danger" style="width: 100%">Cancel</a>
      </div>
    </div>
  </form>
</div>

<?php include "footer.php"?>