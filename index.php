<?php
include 'header.php';
include 'database/db.php';

$produk = $koneksi->query("SELECT * FROM produk")->fetch_all();
?>
<div class="container-fluid" style="margin: 0; padding: 0;">
  <div class="image" style="margin-top: -21px">
    <img src="image/home/1.jpg" style="width: 100%; height: 800px;">
  </div>
</div>
<br>
<br>
<div class="container">
  <h4 class="text-center" style="font-family: arial; padding-top: 10px; padding-bottom: 10px; font-style: italic; line-height: 29px; border-top: 2px solid #ff8d87; border-bottom: 2px solid #ff8d87;"> Skariga Store adalah bisnis kue pertama yang diproduksi oleh sekolah. Didirikan pada tahun 2014, saat ini Skariga Store dikelola di bawah SMK PGRI 3 MALANG.</h4>
  <h2 style="width: 100%; border-bottom: 4px solid #ff8680; margin-top: 80px;"><b>Produk Kami</b></h2>
  <div class="row">
    <?php foreach ($produk as [$kode, $nama, $image, $desc, $harga]) : ?>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img style="object-fit: cover; width: 400px; height: 200px" src="image/produk/<?= $image ?>">
          <div class="caption">
            <h3><?= $nama ?></h3>
            <h4>Rp.<?= number_format($harga, 0, '.', ',') ?></h4>
            <div class="row">
              <div class="col-md-6">
                <a href="detail_produk.php?produk=<?= $kode ?>" class="btn btn-warning btn-block">Detail</a>
              </div>
              <div class="col-md-6">
                <?php if (isset($_SESSION['kd_cs'])) : ?>
                  <a href="proses/add.php?produk=<?= $kode ?>" class="btn btn-success btn-block" role="button"><i class="glyphicon glyphicon-shopping-cart"></i> Tambah</a>
                <?php else : ?>
                  <a href="keranjang.php" class="btn btn-success btn-block" role="button"><i class="glyphicon glyphicon-shopping-cart"></i> Tambah</a>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<br>
<br>
<br>
<br>
<?php
include 'footer.php';
?>